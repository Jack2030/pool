String.prototype.toCharArray = function() {
    var arr = new Array();
    for (var i = 0; i < this.length; i++) {
        arr[i] = this.charAt(i);
    }
    return arr;
};
/**
 * 解析36进制字符
 * @returns {number} 十进制数值
 */
String.prototype.parse36Character = function() {
    var num = 0;
    var charArray = this.toCharArray().reverse();
    for (var i in charArray) {
        var cv = 0;
        if (charArray[i] >= '0' && charArray[i] <= '9') {
            cv = charArray[i].charCodeAt() - 48;
        } else {
            cv = charArray[i].charCodeAt() - 97 + 10;
        }
        num += cv * Math.pow(36, i);
    }
    return num;
};
/**
 * 生成36进制字符
 * @returns {string} 36进制字符串
 */
Number.prototype.generate36Character = function() {
    var c = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
        'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    ];
    var arr = [];
    var num = this;
    while (num) {
        var res = num % 36;
        //下标，转换36进制字符
        arr.unshift(c[res]);
        //去个位
        num = parseInt(num / 36);
    }
    return arr.join("");
};

/**--- QueryString ----**/
function QueryString(url) {
    //获取Url参数集合
    this.qpArray = {};
    if (url && url.indexOf("?") != -1) {
        var qs = url.split("?", 2)[1];
        var qpsArray = qs.split("&");
        for (var qps of qpsArray) {
            if ("" == qps) {
                continue;
            }
            var qp = qps.split('=', 2);
            this.qpArray[qp[0]] = decodeURIComponent(qp[1]);
        }
    }
    this.url = url;
}
QueryString.prototype.searchs = function(keys) {
//   console.log("QueryString srarchs qpArray", this.qpArray);
//   console.log("QueryString srarchs keys", keys);
    //a - appid-appname-channel-inorout
    var arg = this.qpArray['a'];
    var pageParams = arg ? arg.split('$') : new Array();

    var args = {};
    for (var keyIndex in keys) {
        if (pageParams.length > keyIndex) {
            args[keys[keyIndex]] = pageParams[keyIndex];
        }
        else {
            args[keys[keyIndex]] = undefined;
        }
    }
    return args;
};
QueryString.prototype.search = function(key) {
    return this.qpArray[key];
};
/**--- QueryString ----**/

/**--- QueryDomain ----**/
function QueryDomain() {
    this.region = 1;//区域(0:国内，1:国际),默认:国际
    this.name = "h5OutDomainList";
    this.domains = [];//域名池
};
QueryDomain.prototype.syncQuery = function() {
    var queryDomain = this;
    return new Promise((resolve, reject) => {
        var geoip2Query = new Promise((resolve, reject) => {
            if (geoip2) {
                geoip2.setLocationIp('127.0.0.1')
                geoip2.country(function (geoipResponse) {
                    var successRes = geoipResponse.country.iso_code;
                    console.log("QueryDomain region", successRes);
                    // 设置设备地区
                    if (successRes && successRes.toLowerCase() == 'cn') {
                        queryDomain.region = 0;
                    }
                    resolve();
                }, function (error) {
                    resolve();
                });
            } else {
                resolve();
            }
        });

        geoip2Query.then(() => {
            // console.log("QueryDomain region", queryDomain.region);
            queryDomain.name = 0 == queryDomain.region ? "h5InDomainList" : "h5OutDomainList";
            let api = document.getElementById('ZG9tYWlu') // api域名
            api && (queryDomain.name = 'ZG9tYWlu')
            var domainsDev = document.getElementById(queryDomain.name);
            // console.log("QueryDomain name", queryDomain.name,domainsDev);
            if (domainsDev) {
                queryDomain.domains = atob(domainsDev.innerText.trim()).split(",");
            }
            // console.log("QueryDomain domains", this.domains);d
            resolve();
        });
    });
};
/**--- QueryDomain ----**/
var brandDomain = null;

window.onload = function () {

    //queryString
    //a - appid
    //b - appname
    //c - channel
    //d - brand 地址
    var queryString = new QueryString(window.location.href);
    console.log("parse querystring");
    var pageParams = queryString.searchs(['a','b', 'c', 'd','e']);
    //pageParams verify
    //appname => title
    document.title = pageParams['b'];
    const appname = document.getElementById("appname");
    if (pageParams['b'] != undefined && appname) {
        appname.innerText = pageParams['b'];
    }
    //appid
    console.log(pageParams);
    var warning = document.getElementById("warning");
    if (pageParams['a'] == undefined) {
        console.log('应用已下架');
        if (warning) {
            warning.innerText = "未知应用,请重新安装.";
        }
        return;
    }
    const appid = typeof(pageParams['a']) == 'number' ? pageParams['a'] : parseInt(pageParams['a']);
    const appCode = appid.generate36Character();
    //arg
    if (pageParams['c'] == undefined) {
        console.log('应用参数丢失，请重新安装.');
        if (warning) {
            warning.innerText = "应用参数丢失,请重新安装.";
        }
        return;
    }
    // 兼容原来已安装的轻量包，针对没有国内清凉包参数的增加一个参数
    var cTemp = atob(pageParams['c']);
    cTemp = cTemp + 'false##';
    pageParams['c'] = btoa(cTemp);
    const arg = pageParams['c'];
    

    //load cover from cache
    console.log("load cover from cache");
    var coverUrl = localStorage.getItem(appCode + "-Cover");
    if(coverUrl && coverUrl.includes('data:image/')){
        // base64
        $("body").css({"background-image": "url('" + coverUrl + "')", "background-repeat": "no-repeat", "background-size": "cover"});
    }

    if (coverUrl && /^https?:\/\//.test(coverUrl)) {
        // 没解密的地址
        loadCover(appCode, coverUrl)
    }

    //queryDomain
    var queryDomain = new QueryDomain();

    queryDomain.syncQuery().then(() => {
        // console.log(queryDomain.name);
        // console.log(queryDomain.domains);
        
        var domains = queryDomain.domains;
       

        // 处理brand域名，如果没有D就是原来老的清凉包，采用appcode来生成域名，如果有d参数，d参与域名检验
        if (pageParams['d']) {
            let domainSplit = pageParams['d'].split('//');
            let brandDomain = `${domainSplit.length < 2 ? 'https:' : domainSplit[0]}//${appCode}-brand.${pageParams['d'].split('-brand.')[1]}`
            domains.unshift(brandDomain)    // d参数的备用拼接地址
            domains.unshift(pageParams['d'])  // d参数的真实地址
            // var lsdomain = "https://i-brand.xliyoule.app";
            // let brandDomain = `${domainSplit.length < 2 ? 'https:' : domainSplit[0]}//${appCode}-brand.${lsdomain.split('-brand.')[1]}`
            // domains.push(brandDomain)    // d参数的备用拼接地址
        }

        //load core of realtime，and jump app
        // 如果有d参数，直接请求downloadinfo接口
        loadAndCacheAppCover(domains, appid, appCode, arg, (dealUrl) => {
            console.log("dealUrl",dealUrl)
            if (dealUrl) {
                window.location.replace(dealUrl)
            } else {
                console.log('应用域名更新中');
                if (warning) {
                    warning.innerText = "应用域名更新中.";
                }
            }
        }, pageParams['e'])
    });
};

 //load cover of url, and set cover to cache
 var loadCover = function(appCode, coverUrl) {
    return new Promise((resolve, reject) => {
        if (window.fetchAndDec) {
            var suffix = coverUrl.substring(coverUrl.lastIndexOf(".") + 1);
            // console.log(appCode, coverUrl);
            window.fetchAndDec(coverUrl).then(content => {
                var base64Image = content ? `data:image/`+suffix+`;base64,` + content : "";
                //cache cover
                localStorage.setItem(appCode + "-Cover", base64Image);
                //load cover
                $("body").css({"background-image": "url('" + base64Image + "')", "background-repeat": "no-repeat", "background-size": "cover"});
                resolve();
            });
        }
        else {
            resolve();
        }
    });
};

var queryCover = function(url, data, appCode) {

    //sync load app of downloadinfo
    return new Promise((resolve, reject) => {
        jQuery.ajax({
            url: url,
            async: false,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: data,
            success: function (res) {
                // console.log("load cover queryDownloadInfo res", res);
                // console.log("load cover queryDownloadInfo res", typeof res.data);
                if (res.data && res.data["landPageImgUrl"] ) {
                    console.log("load cover queryDownloadInfo landPageImgUrl", res.data["landPageImgUrl"]);
                    // resolve(res.data["landPageImgUrl"]);
                    //exec load and cache cover
                    localStorage.setItem(appCode + "-Cover", res.data["landPageImgUrl"]);
                    loadCover(appCode, res.data["landPageImgUrl"])
                    resolve(res);
                }
                else {
                    resolve(res);
                }
            },
            fail: function (res) {
                console.log('load cover queryDownloadInfo->fail',res);
                reject();
            },
            error: function(res) {
                console.log('load cover queryDownloadInfo->error',res);
                reject();
            }
        });
    });
};

var loadAndCacheAppCover = function(domains, appid, appCode, arg, cb, clickid) {
    //query and store cover
    // ##{channel}##{invitcode}##{appid}##{region}##
    var params = base64.decode(arg).split("##");
    var channel = undefined;
    if (params && params.length > 1) {
        this.channel = params[1];
    }
    deepQueryDownloadInfo(domains, 0, appid, appCode, arg, cb, clickid)
};

let deepQueryDownloadInfo = function (domains, index, appid, appCode, arg, cb, clickid) {
    console.log(domains,"_____domains");
    if(index >= domains.length) {
        cb();
        return
    }
    let domain = domains[index];
    let domainSplit = domain.split('//');
    if(domainSplit.length < 2 ) {
        domainSplit = ['https:', domain];
    }
    if (!domain.includes('-brand')) {
        domain = domainSplit[0] + "//" + appCode + "-brand" + "." + domainSplit[1];
    }
    var queryCoverUrl = domain + "/api/open/applet/landing/queryDownloadInfo";
    console.log("load cover queryDownloadInfo url", queryCoverUrl);
    var queryCoverData = { data: { packageCode: this.channel } };
    queryCoverData = JSON.stringify(queryCoverData);
    queryCover(queryCoverUrl, queryCoverData, appCode).then((res) => {
        if(res.code=='0000') {
            let dealUrl = res.data.htmlDomainUrl + "?a=" + arg;
            clickid && (dealUrl += ('&clickid=' + clickid))
            console.log("dealUrl resolve",dealUrl)
            cb(dealUrl);
        }else{
            deepQueryDownloadInfo(domains, index + 1, appid, appCode, arg, cb, clickid)
        }
    }).catch((err) => {
        deepQueryDownloadInfo(domains, index + 1, appid, appCode, arg, cb, clickid);
    });
}
