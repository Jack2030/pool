String.prototype.toCharArray = function() {
    var arr = new Array();
    for (var i = 0; i < this.length; i++) {
        arr[i] = this.charAt(i);
    }
    return arr;
};
/**
 * 解析36进制字符
 * @returns {number} 十进制数值
 */
String.prototype.parse36Character = function() {
    var num = 0;
    var charArray = this.toCharArray().reverse();
    for (var i in charArray) {
        var cv = 0;
        if (charArray[i] >= '0' && charArray[i] <= '9') {
            cv = charArray[i].charCodeAt() - 48;
        } else {
            cv = charArray[i].charCodeAt() - 97 + 10;
        }
        num += cv * Math.pow(36, i);
    }
    return num;
};
/**
 * 生成36进制字符
 * @returns {string} 36进制字符串
 */
Number.prototype.generate36Character = function() {
    var c = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
        'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    ];
    var arr = [];
    var num = this;
    while (num) {
        var res = num % 36;
        //下标，转换36进制字符
        arr.unshift(c[res]);
        //去个位
        num = parseInt(num / 36);
    }
    return arr.join("");
};

/**--- QueryString ----**/
function QueryString(url) {
    //获取Url参数集合
    this.qpArray = {};
    if (url && url.indexOf("?") != -1) {
        var qs = url.split("?", 2)[1];
        var qpsArray = qs.split("&");
        for (var qps of qpsArray) {
            if ("" == qps) {
                continue;
            }
            var qp = qps.split('=', 2);
            this.qpArray[qp[0]] = decodeURIComponent(qp[1]);
        }
    }
    this.url = url;
}
QueryString.prototype.searchs = function(keys) {
//   console.log("QueryString srarchs qpArray", this.qpArray);
//   console.log("QueryString srarchs keys", keys);
    //a - appid-appname-channel-inorout
    var arg = this.qpArray['a'];
    var pageParams = arg ? arg.split('$') : new Array();

    var args = {};
    for (var keyIndex in keys) {
        if (pageParams.length > keyIndex) {
            args[keys[keyIndex]] = pageParams[keyIndex];
        }
        else {
            args[keys[keyIndex]] = undefined;
        }
    }
    return args;
};
QueryString.prototype.search = function(key) {
    return this.qpArray[key];
};
/**--- QueryString ----**/

/**--- QueryDomain ----**/
function QueryDomain() {
    this.region = 1;//区域(0:国内，1:国际),默认:国际
    this.name = "h5OutDomainList";
    this.domains = [];//域名池
};
QueryDomain.prototype.syncQuery = function(fn) {
    var queryDomain = this;

    var callback = false;
    // Geoip2查询
    // 配置maxmind接口参数
    if (geoip2) {
        geoip2.setLocationIp('127.0.0.1')
        geoip2.country(function (geoipResponse) {
            var successRes = geoipResponse.country.iso_code;
            console.log("QueryDomain region", successRes);
            // 设置设备地区
            if (successRes && successRes.toLowerCase() == 'cn') {
                queryDomain.region = 0;
            }
            callback = true;
        }, function (error) {
            callback = true;
        });
    }
    else {
        callback = true;
    }

    var timer = setInterval(function() {
        if (callback) {
//            console.log("QueryDomain region", queryDomain.region);
            queryDomain.name = 0 == queryDomain.region ? "h5InDomainList" : "h5OutDomainList";
//            console.log("QueryDomain name", queryDomain.name);
            var domainsDev = document.getElementById(queryDomain.name);
            if (domainsDev) {
                queryDomain.domains = atob(domainsDev.innerText.trim()).split(",");
            }
//            console.log("QueryDomain domains", this.domains);
            clearInterval(timer);
            if (fn) { fn(); }
        }
    }, 100);
};
/**--- QueryDomain ----**/

/**--- DomainVerify ----**/
function DomainVerfy(domains, appid, channel) {
    this.domains = [...domains];
    this.appid = typeof(appid) == 'number' ? appid : parseInt(appid);
    this.parseCode = this.appid.generate36Character();
    this.urlPre = this.parseCode + "-brand";
    this.channel = channel;
    this.init = function() {
        let domainSplit = this.domains[0].split('//');
        if( domainSplit.length < 2 ) {
            domainSplit = ['https:', this.domains[0]];
        }
        //eg:http://uv-brand.saas-xy.com:82/?a=IyNrb3JpbjYjIyMjMTExMSMj
        this.dealUrl = domainSplit[0] + "//" + this.urlPre + "." + domainSplit[1] + "?a=" + this.channel;
//        console.log("DomainVerfy init dealUrl", this.dealUrl);
    };
    // this.init();
    this.getDomainList = function(domain) {
//        console.log("DomainVerfy getDomainList domain", domain);
        let domainSplit = domain.split('//');
        if( domainSplit.length < 2 ) {
            domainSplit = ['https:', domain];
        }

        //eg:http://uv-brand.saas-xy.com:82/api/open/applet/queryDomain
        var nowUrl = domainSplit[0] + "//" + this.urlPre + "." + domainSplit[1];
//        console.log("DomainVerfy getDomainList nowUrl", nowUrl);

        var nowData = { data: { appId: this.appid, origin: 11 } };
//        console.log("DomainVerfy getDomainList nowData", nowData);
        nowData = JSON.stringify(nowData);

        var result = false;
        //domain test request
        jQuery.ajax({
            url: nowUrl + "/api/open/applet/queryDomainV2",
            async: false,
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: nowData,
            success: function (res) {
                result = res.data ? res.data : false;
//               console.log("DomainVerfy getDomainList res", res);
//               console.log("DomainVerfy getDomainList res", typeof res.data, typeof res.data == 'string');
                if (res.data && typeof res.data == 'string') {
                    let tempDomainArr = atob(res.data).split(',');
//                    console.log("DomainVerfy getDomainList tempDomainArr", tempDomainArr);
                    // console.log('域名加密数据---',res,tempDomainArr);
                    // return resolve(res.data)
                    if (tempDomainArr) {
                        result = tempDomainArr;
                    }
                }
            },
            fail: function (res) {
                console.log('DomainVerfy getDomainList->fail',res)
            }
        });
        return result;
    };
};
DomainVerfy.prototype.verfy = function() {
    for (var nowDomain of this.domains) {
        const getDomains = this.getDomainList(nowDomain);
        console.log("DomainVerfy verfy", getDomains);
        if (getDomains) {
            this.domains = [...getDomains];
            this.init();
            return true;
        }
    }
    return false;
};
/**--- DomainVerify ----**/

window.onload = function () {
    //queryString
    var queryString = new QueryString(window.location.href);
    //a - appid
    //b - appname
    //c - channel
    //d - 0:in or 1:out
    var pageParams = queryString.searchs(['a','b', 'c']);
    //pageParams appname:title
    document.title = pageParams['b'];
    var appname = document.getElementById("appname");
    if (pageParams['b'] != undefined && appname) {
        appname.innerText = pageParams['b'];
    }
    //pageParams verify
    console.log(pageParams);
    var warning = document.getElementById("warning");
    if (pageParams['a'] == undefined) {
        console.log('应用已下架');
        if (warning) {
            warning.innerText = "未知应用,请重新安装.";
        }
        return;
    }
    pageParams['a'] = parseInt(pageParams['a']);
    if (pageParams['c'] == undefined) {
        console.log('应用参数丢失，请重新安装.');
        if (warning) {
            warning.innerText = "应用参数丢失,请重新安装.";
        }
        return;
    }

    // 兼容原来已安装的轻量包，针对没有国内清凉包参数的增加一个参数
    var cTemp = atob(pageParams['c']);
    cTemp = cTemp + 'false##';
    pageParams['c'] = btoa(cTemp);

    //queryDomain
    var queryDomain = new QueryDomain();
    queryDomain.syncQuery(function() {
//        console.log(queryDomain.name);
        console.log(queryDomain.domains);

        //try to jump
        var domainVerfy = new DomainVerfy(queryDomain.domains, pageParams['a'], pageParams['c']);
        if (domainVerfy.verfy()) {
            console.log(domainVerfy.dealUrl);
            window.location.replace(domainVerfy.dealUrl)
        }
        else {
            console.log('应用域名更新中');
            if (warning) {
                warning.innerText = "应用域名更新中.";
            }
        }
    });
};
