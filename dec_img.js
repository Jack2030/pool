(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
  "use strict";
  
  var _runtime = _interopRequireDefault(require("./runtime"));
  
  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
  
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
  
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }
  
  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }
  
  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
  
  function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
  
  var Hour = 3600;
  var Day = 24 * Hour;
  var Week = 30 * Day;
	const TIME_OUT=180*1000
	/**
	 * fetch 网络请求超时处理
	 * @param original_promise 原始的fetch
	 * @param timeout 超时时间 30s
	 * @returns {Promise.<*>}
	 */
	const timeoutFetch = (original_fetch, timeout = TIME_OUT) => {
		const timeout_promise = new Promise((resolve, reject) => {
			setTimeout(() => {
				reject('timeout')
			}, timeout);
		})
		return Promise.race([original_fetch,timeout_promise])
	}

  function createCache(storage, opts) {
    var getter = opts.getter,
        setter = opts.setter;
  
    if (!getter || !setter) {
      throw new Error('create Cache must off getter and setter');
    }
  
    function save(_x, _x2) {
      return _save.apply(this, arguments);
    }
  
    function _save() {
      _save = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee(key, value) {
        var currentTimestamp;
        return _runtime["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                currentTimestamp = Date.now() / 1000 | 0;
                return _context.abrupt("return", setter(storage, key, {
                  value: value,
                  timestamp: currentTimestamp
                }));
  
              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));
      return _save.apply(this, arguments);
    }
  
    function load(_x3) {
      return _load.apply(this, arguments);
    } // callWith('fetchName', fetchName, 3000, [id, token])
  
  
    function _load() {
      _load = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee2(key) {
        var ttl,
            currentTimestamp,
            box,
            timestamp,
            _args2 = arguments;
        return _runtime["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                ttl = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : Day;
                currentTimestamp = Date.now() / 1000 | 0;
                _context2.next = 4;
                return getter(storage, key);
  
              case 4:
                box = _context2.sent;
  
                if (!box) {
                  _context2.next = 9;
                  break;
                }
  
                timestamp = box.timestamp || 0;
  
                if (!(currentTimestamp - timestamp < ttl)) {
                  _context2.next = 9;
                  break;
                }
  
                return _context2.abrupt("return", box.value);
  
              case 9:
                return _context2.abrupt("return", undefined);
  
              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));
      return _load.apply(this, arguments);
    }
  
    function applyWith(_x4, _x5, _x6, _x7) {
      return _applyWith.apply(this, arguments);
    } // let fetchNameWithCache = wrapperWith('fetchName', fetchName, 3000)
    // fetchNameWithCache(id, token)
  
  
    function _applyWith() {
      _applyWith = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee3(name, func, ttl, params) {
        var key, box, currentTimestamp, timestamp, value;
        return _runtime["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                key = JSON.stringify([name, params]);
                _context3.next = 3;
                return getter(storage, key);
  
              case 3:
                box = _context3.sent;
                currentTimestamp = Date.now() / 1000 | 0; // call with cache
  
                if (!box) {
                  _context3.next = 9;
                  break;
                }
  
                timestamp = box.timestamp || 0;
  
                if (!(currentTimestamp - timestamp < ttl)) {
                  _context3.next = 9;
                  break;
                }
  
                return _context3.abrupt("return", box.value);
  
              case 9:
                _context3.next = 11;
                return func.apply(null, params);
  
              case 11:
                value = _context3.sent;
                // do not to await setter
                setter(storage, key, {
                  value: value,
                  timestamp: currentTimestamp
                });
                return _context3.abrupt("return", value);
  
              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));
      return _applyWith.apply(this, arguments);
    }
  
    function wrapperWithCall(name, func, ttl) {
      return function () {
        for (var _len = arguments.length, params = new Array(_len), _key = 0; _key < _len; _key++) {
          params[_key] = arguments[_key];
        }
  
        return applyWith(name, func, ttl, params);
      };
    }
  
    function cleanUp() {
      return _cleanUp.apply(this, arguments);
    }
  
    function _cleanUp() {
      _cleanUp = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee4() {
        var ttl,
            _args4 = arguments;
        return _runtime["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                ttl = _args4.length > 0 && _args4[0] !== undefined ? _args4[0] : Day;
  
                if (!(!opts.iterater || !opts.remover)) {
                  _context4.next = 4;
                  break;
                }
  
                console.warn('create cache do not offer iterater and remover function');
                return _context4.abrupt("return", false);
  
              case 4:
                _context4.next = 6;
                return opts.iterater(storage, function (v, k) {
                  if (!v || !v.value || !v.timestamp) {
                    opts.remover(storage, k);
                  } else {
                    var currentTimestamp = Date.now() / 1000 | 0;
  
                    if (currentTimestamp - v.timestamp > ttl) {
                      opts.remover(storage, k);
                    }
                  }
                });
  
              case 6:
                return _context4.abrupt("return", true);
  
              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));
      return _cleanUp.apply(this, arguments);
    }
  
    return {
      applyWith: applyWith,
      wrapperWithCall: wrapperWithCall,
      cleanUp: cleanUp,
      save: save,
      load: load
    };
  }
  
  var MemoryStorage = /*#__PURE__*/function () {
    function MemoryStorage() {
      var maxCount = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 200;
  
      _classCallCheck(this, MemoryStorage);
  
      this.dict = {};
      this.queue = [];
      this.maxCount = maxCount;
    }
  
    _createClass(MemoryStorage, [{
      key: "getItem",
      value: function getItem(key) {
        return this.dict[key];
      }
    }, {
      key: "setItem",
      value: function setItem(key, value) {
        if (this.queue.length > this.maxCount) {
          var deleteKey = this.queue.shift();
          delete this.dict[deleteKey];
        }
  
        this.dict[key] = value;
      }
    }]);
  
    return MemoryStorage;
  }(); // mock storage and cache
  // 此处会被服务端使用
  
  
  var mockStorage = {
    dict: {},
    getItem: function getItem(key) {
      return this.dict[key];
    },
    setItem: function setItem(key, value) {
      this.dict[key] = value;
    }
  };
  var noCache = createCache(mockStorage, {
    getter: function () {
      var _getter = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee5(storage, key) {
        return _runtime["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                return _context5.abrupt("return", false);
  
              case 1:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));
  
      function getter(_x8, _x9) {
        return _getter.apply(this, arguments);
      }
  
      return getter;
    }(),
    setter: function () {
      var _setter = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee6(storage, key, value) {
        return _runtime["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));
  
      function setter(_x10, _x11, _x12) {
        return _setter.apply(this, arguments);
      }
  
      return setter;
    }()
  });
  var mockCache = createCache(mockStorage, {
    getter: function () {
      var _getter2 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee7(storage, key) {
        return _runtime["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                return _context7.abrupt("return", storage.getItem(key));
  
              case 1:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));
  
      function getter(_x13, _x14) {
        return _getter2.apply(this, arguments);
      }
  
      return getter;
    }(),
    setter: function () {
      var _setter2 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee8(storage, key, value) {
        return _runtime["default"].wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                return _context8.abrupt("return", storage.setItem(key, value));
  
              case 1:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));
  
      function setter(_x15, _x16, _x17) {
        return _setter2.apply(this, arguments);
      }
  
      return setter;
    }()
  });
  var memoryCache = createCache(new MemoryStorage(500), {
    getter: function () {
      var _getter3 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee9(storage, key) {
        return _runtime["default"].wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                return _context9.abrupt("return", storage.getItem(key));
  
              case 1:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }));
  
      function getter(_x18, _x19) {
        return _getter3.apply(this, arguments);
      }
  
      return getter;
    }(),
    setter: function () {
      var _setter3 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee10(storage, key, value) {
        return _runtime["default"].wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                return _context10.abrupt("return", storage.setItem(key, value));
  
              case 1:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }));
  
      function setter(_x20, _x21, _x22) {
        return _setter3.apply(this, arguments);
      }
  
      return setter;
    }()
  }); // localStorage and cache
  
  var lsCache;
  
  if (typeof window === 'undefined') {
    lsCache = noCache;
  } else {
    lsCache = createCache(window.localStorage, {
      getter: function () {
        var _getter4 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee11(storage, key) {
          return _runtime["default"].wrap(function _callee11$(_context11) {
            while (1) {
              switch (_context11.prev = _context11.next) {
                case 0:
                  _context11.prev = 0;
                  return _context11.abrupt("return", JSON.parse(storage.getItem(key)));
  
                case 4:
                  _context11.prev = 4;
                  _context11.t0 = _context11["catch"](0);
                  return _context11.abrupt("return", false);
  
                case 7:
                case "end":
                  return _context11.stop();
              }
            }
          }, _callee11, null, [[0, 4]]);
        }));
  
        function getter(_x23, _x24) {
          return _getter4.apply(this, arguments);
        }
  
        return getter;
      }(),
      setter: function () {
        var _setter4 = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee12(storage, key, value) {
          return _runtime["default"].wrap(function _callee12$(_context12) {
            while (1) {
              switch (_context12.prev = _context12.next) {
                case 0:
                  return _context12.abrupt("return", storage.setItem(key, JSON.stringify(value)));
  
                case 1:
                case "end":
                  return _context12.stop();
              }
            }
          }, _callee12);
        }));
  
        function setter(_x25, _x26, _x27) {
          return _setter4.apply(this, arguments);
        }
  
        return setter;
      }()
    });
  }
  
  function decode(data) {
    var DECODE_MASK = -1;
    var DECODE_LENGTH = 5000;
    var mask = -1;
    var offset = 0,
        result_offset = 0;
  
    while (offset < data.byteLength) {
      if (mask === DECODE_MASK) {
        mask = data[offset++];
      }
  
      var count = DECODE_LENGTH;
  
      if (mask === DECODE_MASK) {
        mask = data[offset++];
        --count;
      }
  
      var i = 0;
  
      for (; i < count && offset + i < data.byteLength; ++i) {
        data[result_offset + i] = data[offset + i] ^ mask;
      }
  
      offset += i;
      result_offset += i;
    }
  
    return result_offset;
  }
  
  var arrayBufferToBase64 = function arrayBufferToBase64(bytes) {
    // 转换数组为 base64 编码数据
    var binary = '';
    var len = bytes.byteLength;
  
    for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
  
    return window.btoa(binary); // TODO:
  };
  
  function imgPromiseDec(imgData) {
    return new Promise(function (resolve) {
      var buf = new Uint8Array(imgData);
      var len = decode(buf);
      buf = buf.slice(0, len);
      resolve(arrayBufferToBase64(buf));
    });
  }
  
  function rawFetchAndDec(_x28) {
    return _rawFetchAndDec.apply(this, arguments);
  }
  
  function _rawFetchAndDec() {
    _rawFetchAndDec = _asyncToGenerator( /*#__PURE__*/_runtime["default"].mark(function _callee13(esrc) {
      var abuffer, str;
      return _runtime["default"].wrap(function _callee13$(_context13) {
        while (1) {
          switch (_context13.prev = _context13.next) {
            case 0:
              _context13.next = 2;
              return timeoutFetch(fetch(esrc,{referrer:"",referrerPolicy:"no-referrer"})).then(function (res) {
								if (res.status==200) {
									return res.arrayBuffer();
								} else {
									return Promise.reject(res.statusText)
								}
              }).catch(err=>{
								return Promise.reject(err)
							});
  
            case 2:
              abuffer = _context13.sent;
              _context13.next = 5;
              return imgPromiseDec(abuffer);
  
            case 5:
              str = _context13.sent;
              return _context13.abrupt("return", str);
  
            case 7:
            case "end":
              return _context13.stop();
          }
        }
      }, _callee13);
    }));
    return _rawFetchAndDec.apply(this, arguments);
  }
  
  window.fetchAndDec = memoryCache.wrapperWithCall('fetchAndDec', rawFetchAndDec, 3600 * 24 * 7); // fetchAndDec('http://35.220.167.29:18060/mback/huluwa/img/20200509/56b7f9e864293ac5e41f3939337737c5.png').then(aaa => { console.log(aaa) })
  //   .catch(e => { alert(e) })
  
  },{"./runtime":2}],2:[function(require,module,exports){
  "use strict";
  
  function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }
  
  /**
   * Copyright (c) 2014-present, Facebook, Inc.
   *
   * This source code is licensed under the MIT license found in the
   * LICENSE file in the root directory of this source tree.
   */
  var runtime = function (exports) {
    "use strict";
  
    var Op = Object.prototype;
    var hasOwn = Op.hasOwnProperty;
    var undefined; // More compressible than void 0.
  
    var $Symbol = typeof Symbol === "function" ? Symbol : {};
    var iteratorSymbol = $Symbol.iterator || "@@iterator";
    var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
    var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";
  
    function wrap(innerFn, outerFn, self, tryLocsList) {
      // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
      var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
      var generator = Object.create(protoGenerator.prototype);
      var context = new Context(tryLocsList || []); // The ._invoke method unifies the implementations of the .next,
      // .throw, and .return methods.
  
      generator._invoke = makeInvokeMethod(innerFn, self, context);
      return generator;
    }
  
    exports.wrap = wrap; // Try/catch helper to minimize deoptimizations. Returns a completion
    // record like context.tryEntries[i].completion. This interface could
    // have been (and was previously) designed to take a closure to be
    // invoked without arguments, but in all the cases we care about we
    // already have an existing method we want to call, so there's no need
    // to create a new function object. We can even get away with assuming
    // the method takes exactly one argument, since that happens to be true
    // in every case, so we don't have to touch the arguments object. The
    // only additional allocation required is the completion record, which
    // has a stable shape and so hopefully should be cheap to allocate.
  
    function tryCatch(fn, obj, arg) {
      try {
        return {
          type: "normal",
          arg: fn.call(obj, arg)
        };
      } catch (err) {
        return {
          type: "throw",
          arg: err
        };
      }
    }
  
    var GenStateSuspendedStart = "suspendedStart";
    var GenStateSuspendedYield = "suspendedYield";
    var GenStateExecuting = "executing";
    var GenStateCompleted = "completed"; // Returning this object from the innerFn has the same effect as
    // breaking out of the dispatch switch statement.
  
    var ContinueSentinel = {}; // Dummy constructor functions that we use as the .constructor and
    // .constructor.prototype properties for functions that return Generator
    // objects. For full spec compliance, you may wish to configure your
    // minifier not to mangle the names of these two functions.
  
    function Generator() {}
  
    function GeneratorFunction() {}
  
    function GeneratorFunctionPrototype() {} // This is a polyfill for %IteratorPrototype% for environments that
    // don't natively support it.
  
  
    var IteratorPrototype = {};
  
    IteratorPrototype[iteratorSymbol] = function () {
      return this;
    };
  
    var getProto = Object.getPrototypeOf;
    var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  
    if (NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
      // This environment has a native %IteratorPrototype%; use it instead
      // of the polyfill.
      IteratorPrototype = NativeIteratorPrototype;
    }
  
    var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);
    GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
    GeneratorFunctionPrototype.constructor = GeneratorFunction;
    GeneratorFunctionPrototype[toStringTagSymbol] = GeneratorFunction.displayName = "GeneratorFunction"; // Helper for defining the .next, .throw, and .return methods of the
    // Iterator interface in terms of a single ._invoke method.
  
    function defineIteratorMethods(prototype) {
      ["next", "throw", "return"].forEach(function (method) {
        prototype[method] = function (arg) {
          return this._invoke(method, arg);
        };
      });
    }
  
    exports.isGeneratorFunction = function (genFun) {
      var ctor = typeof genFun === "function" && genFun.constructor;
      return ctor ? ctor === GeneratorFunction || // For the native GeneratorFunction constructor, the best we can
      // do is to check its .name property.
      (ctor.displayName || ctor.name) === "GeneratorFunction" : false;
    };
  
    exports.mark = function (genFun) {
      if (Object.setPrototypeOf) {
        Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
      } else {
        genFun.__proto__ = GeneratorFunctionPrototype;
  
        if (!(toStringTagSymbol in genFun)) {
          genFun[toStringTagSymbol] = "GeneratorFunction";
        }
      }
  
      genFun.prototype = Object.create(Gp);
      return genFun;
    }; // Within the body of any async function, `await x` is transformed to
    // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
    // `hasOwn.call(value, "__await")` to determine if the yielded value is
    // meant to be awaited.
  
  
    exports.awrap = function (arg) {
      return {
        __await: arg
      };
    };
  
    function AsyncIterator(generator, PromiseImpl) {
      function invoke(method, arg, resolve, reject) {
        var record = tryCatch(generator[method], generator, arg);
  
        if (record.type === "throw") {
          reject(record.arg);
        } else {
          var result = record.arg;
          var value = result.value;
  
          if (value && _typeof(value) === "object" && hasOwn.call(value, "__await")) {
            return PromiseImpl.resolve(value.__await).then(function (value) {
              invoke("next", value, resolve, reject);
            }, function (err) {
              invoke("throw", err, resolve, reject);
            });
          }
  
          return PromiseImpl.resolve(value).then(function (unwrapped) {
            // When a yielded Promise is resolved, its final value becomes
            // the .value of the Promise<{value,done}> result for the
            // current iteration.
            result.value = unwrapped;
            resolve(result);
          }, function (error) {
            // If a rejected Promise was yielded, throw the rejection back
            // into the async generator function so it can be handled there.
            return invoke("throw", error, resolve, reject);
          });
        }
      }
  
      var previousPromise;
  
      function enqueue(method, arg) {
        function callInvokeWithMethodAndArg() {
          return new PromiseImpl(function (resolve, reject) {
            invoke(method, arg, resolve, reject);
          });
        }
  
        return previousPromise = // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, // Avoid propagating failures to Promises returned by later
        // invocations of the iterator.
        callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
      } // Define the unified helper method that is used to implement .next,
      // .throw, and .return (see defineIteratorMethods).
  
  
      this._invoke = enqueue;
    }
  
    defineIteratorMethods(AsyncIterator.prototype);
  
    AsyncIterator.prototype[asyncIteratorSymbol] = function () {
      return this;
    };
  
    exports.AsyncIterator = AsyncIterator; // Note that simple async functions are implemented on top of
    // AsyncIterator objects; they just return a Promise for the value of
    // the final result produced by the iterator.
  
    exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
      if (PromiseImpl === void 0) PromiseImpl = Promise;
      var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
      return exports.isGeneratorFunction(outerFn) ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function (result) {
        return result.done ? result.value : iter.next();
      });
    };
  
    function makeInvokeMethod(innerFn, self, context) {
      var state = GenStateSuspendedStart;
      return function invoke(method, arg) {
        if (state === GenStateExecuting) {
          throw new Error("Generator is already running");
        }
  
        if (state === GenStateCompleted) {
          if (method === "throw") {
            throw arg;
          } // Be forgiving, per 25.3.3.3.3 of the spec:
          // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
  
  
          return doneResult();
        }
  
        context.method = method;
        context.arg = arg;
  
        while (true) {
          var delegate = context.delegate;
  
          if (delegate) {
            var delegateResult = maybeInvokeDelegate(delegate, context);
  
            if (delegateResult) {
              if (delegateResult === ContinueSentinel) continue;
              return delegateResult;
            }
          }
  
          if (context.method === "next") {
            // Setting context._sent for legacy support of Babel's
            // function.sent implementation.
            context.sent = context._sent = context.arg;
          } else if (context.method === "throw") {
            if (state === GenStateSuspendedStart) {
              state = GenStateCompleted;
              throw context.arg;
            }
  
            context.dispatchException(context.arg);
          } else if (context.method === "return") {
            context.abrupt("return", context.arg);
          }
  
          state = GenStateExecuting;
          var record = tryCatch(innerFn, self, context);
  
          if (record.type === "normal") {
            // If an exception is thrown from innerFn, we leave state ===
            // GenStateExecuting and loop back for another invocation.
            state = context.done ? GenStateCompleted : GenStateSuspendedYield;
  
            if (record.arg === ContinueSentinel) {
              continue;
            }
  
            return {
              value: record.arg,
              done: context.done
            };
          } else if (record.type === "throw") {
            state = GenStateCompleted; // Dispatch the exception by looping back around to the
            // context.dispatchException(context.arg) call above.
  
            context.method = "throw";
            context.arg = record.arg;
          }
        }
      };
    } // Call delegate.iterator[context.method](context.arg) and handle the
    // result, either by returning a { value, done } result from the
    // delegate iterator, or by modifying context.method and context.arg,
    // setting context.delegate to null, and returning the ContinueSentinel.
  
  
    function maybeInvokeDelegate(delegate, context) {
      var method = delegate.iterator[context.method];
  
      if (method === undefined) {
        // A .throw or .return when the delegate iterator has no .throw
        // method always terminates the yield* loop.
        context.delegate = null;
  
        if (context.method === "throw") {
          // Note: ["return"] must be used for ES3 parsing compatibility.
          if (delegate.iterator["return"]) {
            // If the delegate iterator has a return method, give it a
            // chance to clean up.
            context.method = "return";
            context.arg = undefined;
            maybeInvokeDelegate(delegate, context);
  
            if (context.method === "throw") {
              // If maybeInvokeDelegate(context) changed context.method from
              // "return" to "throw", let that override the TypeError below.
              return ContinueSentinel;
            }
          }
  
          context.method = "throw";
          context.arg = new TypeError("The iterator does not provide a 'throw' method");
        }
  
        return ContinueSentinel;
      }
  
      var record = tryCatch(method, delegate.iterator, context.arg);
  
      if (record.type === "throw") {
        context.method = "throw";
        context.arg = record.arg;
        context.delegate = null;
        return ContinueSentinel;
      }
  
      var info = record.arg;
  
      if (!info) {
        context.method = "throw";
        context.arg = new TypeError("iterator result is not an object");
        context.delegate = null;
        return ContinueSentinel;
      }
  
      if (info.done) {
        // Assign the result of the finished delegate to the temporary
        // variable specified by delegate.resultName (see delegateYield).
        context[delegate.resultName] = info.value; // Resume execution at the desired location (see delegateYield).
  
        context.next = delegate.nextLoc; // If context.method was "throw" but the delegate handled the
        // exception, let the outer generator proceed normally. If
        // context.method was "next", forget context.arg since it has been
        // "consumed" by the delegate iterator. If context.method was
        // "return", allow the original .return call to continue in the
        // outer generator.
  
        if (context.method !== "return") {
          context.method = "next";
          context.arg = undefined;
        }
      } else {
        // Re-yield the result returned by the delegate method.
        return info;
      } // The delegate iterator is finished, so forget it and continue with
      // the outer generator.
  
  
      context.delegate = null;
      return ContinueSentinel;
    } // Define Generator.prototype.{next,throw,return} in terms of the
    // unified ._invoke helper method.
  
  
    defineIteratorMethods(Gp);
    Gp[toStringTagSymbol] = "Generator"; // A Generator should always return itself as the iterator object when the
    // @@iterator function is called on it. Some browsers' implementations of the
    // iterator prototype chain incorrectly implement this, causing the Generator
    // object to not be returned from this call. This ensures that doesn't happen.
    // See https://github.com/facebook/regenerator/issues/274 for more details.
  
    Gp[iteratorSymbol] = function () {
      return this;
    };
  
    Gp.toString = function () {
      return "[object Generator]";
    };
  
    function pushTryEntry(locs) {
      var entry = {
        tryLoc: locs[0]
      };
  
      if (1 in locs) {
        entry.catchLoc = locs[1];
      }
  
      if (2 in locs) {
        entry.finallyLoc = locs[2];
        entry.afterLoc = locs[3];
      }
  
      this.tryEntries.push(entry);
    }
  
    function resetTryEntry(entry) {
      var record = entry.completion || {};
      record.type = "normal";
      delete record.arg;
      entry.completion = record;
    }
  
    function Context(tryLocsList) {
      // The root entry object (effectively a try statement without a catch
      // or a finally block) gives us a place to store values thrown from
      // locations where there is no enclosing try statement.
      this.tryEntries = [{
        tryLoc: "root"
      }];
      tryLocsList.forEach(pushTryEntry, this);
      this.reset(true);
    }
  
    exports.keys = function (object) {
      var keys = [];
  
      for (var key in object) {
        keys.push(key);
      }
  
      keys.reverse(); // Rather than returning an object with a next method, we keep
      // things simple and return the next function itself.
  
      return function next() {
        while (keys.length) {
          var key = keys.pop();
  
          if (key in object) {
            next.value = key;
            next.done = false;
            return next;
          }
        } // To avoid creating an additional object, we just hang the .value
        // and .done properties off the next function object itself. This
        // also ensures that the minifier will not anonymize the function.
  
  
        next.done = true;
        return next;
      };
    };
  
    function values(iterable) {
      if (iterable) {
        var iteratorMethod = iterable[iteratorSymbol];
  
        if (iteratorMethod) {
          return iteratorMethod.call(iterable);
        }
  
        if (typeof iterable.next === "function") {
          return iterable;
        }
  
        if (!isNaN(iterable.length)) {
          var i = -1,
              next = function next() {
            while (++i < iterable.length) {
              if (hasOwn.call(iterable, i)) {
                next.value = iterable[i];
                next.done = false;
                return next;
              }
            }
  
            next.value = undefined;
            next.done = true;
            return next;
          };
  
          return next.next = next;
        }
      } // Return an iterator with no values.
  
  
      return {
        next: doneResult
      };
    }
  
    exports.values = values;
  
    function doneResult() {
      return {
        value: undefined,
        done: true
      };
    }
  
    Context.prototype = {
      constructor: Context,
      reset: function reset(skipTempReset) {
        this.prev = 0;
        this.next = 0; // Resetting context._sent for legacy support of Babel's
        // function.sent implementation.
  
        this.sent = this._sent = undefined;
        this.done = false;
        this.delegate = null;
        this.method = "next";
        this.arg = undefined;
        this.tryEntries.forEach(resetTryEntry);
  
        if (!skipTempReset) {
          for (var name in this) {
            // Not sure about the optimal order of these conditions:
            if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
              this[name] = undefined;
            }
          }
        }
      },
      stop: function stop() {
        this.done = true;
        var rootEntry = this.tryEntries[0];
        var rootRecord = rootEntry.completion;
  
        if (rootRecord.type === "throw") {
          throw rootRecord.arg;
        }
  
        return this.rval;
      },
      dispatchException: function dispatchException(exception) {
        if (this.done) {
          throw exception;
        }
  
        var context = this;
  
        function handle(loc, caught) {
          record.type = "throw";
          record.arg = exception;
          context.next = loc;
  
          if (caught) {
            // If the dispatched exception was caught by a catch block,
            // then let that catch block handle the exception normally.
            context.method = "next";
            context.arg = undefined;
          }
  
          return !!caught;
        }
  
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          var record = entry.completion;
  
          if (entry.tryLoc === "root") {
            // Exception thrown outside of any try block that could handle
            // it, so set the completion value of the entire function to
            // throw the exception.
            return handle("end");
          }
  
          if (entry.tryLoc <= this.prev) {
            var hasCatch = hasOwn.call(entry, "catchLoc");
            var hasFinally = hasOwn.call(entry, "finallyLoc");
  
            if (hasCatch && hasFinally) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              } else if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else if (hasCatch) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              }
            } else if (hasFinally) {
              if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else {
              throw new Error("try statement without catch or finally");
            }
          }
        }
      },
      abrupt: function abrupt(type, arg) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
  
          if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
            var finallyEntry = entry;
            break;
          }
        }
  
        if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
          // Ignore the finally entry if control is not jumping to a
          // location outside the try/catch block.
          finallyEntry = null;
        }
  
        var record = finallyEntry ? finallyEntry.completion : {};
        record.type = type;
        record.arg = arg;
  
        if (finallyEntry) {
          this.method = "next";
          this.next = finallyEntry.finallyLoc;
          return ContinueSentinel;
        }
  
        return this.complete(record);
      },
      complete: function complete(record, afterLoc) {
        if (record.type === "throw") {
          throw record.arg;
        }
  
        if (record.type === "break" || record.type === "continue") {
          this.next = record.arg;
        } else if (record.type === "return") {
          this.rval = this.arg = record.arg;
          this.method = "return";
          this.next = "end";
        } else if (record.type === "normal" && afterLoc) {
          this.next = afterLoc;
        }
  
        return ContinueSentinel;
      },
      finish: function finish(finallyLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
  
          if (entry.finallyLoc === finallyLoc) {
            this.complete(entry.completion, entry.afterLoc);
            resetTryEntry(entry);
            return ContinueSentinel;
          }
        }
      },
      "catch": function _catch(tryLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
  
          if (entry.tryLoc === tryLoc) {
            var record = entry.completion;
  
            if (record.type === "throw") {
              var thrown = record.arg;
              resetTryEntry(entry);
            }
  
            return thrown;
          }
        } // The context.catch method must only be called with a location
        // argument that corresponds to a known catch block.
  
  
        throw new Error("illegal catch attempt");
      },
      delegateYield: function delegateYield(iterable, resultName, nextLoc) {
        this.delegate = {
          iterator: values(iterable),
          resultName: resultName,
          nextLoc: nextLoc
        };
  
        if (this.method === "next") {
          // Deliberately forget the last sent value so that we don't
          // accidentally pass it on to the delegate.
          this.arg = undefined;
        }
  
        return ContinueSentinel;
      }
    }; // Regardless of whether this script is executing as a CommonJS module
    // or not, return the runtime object so that we can declare the variable
    // regeneratorRuntime in the outer scope, which allows this module to be
    // injected easily by `bin/regenerator --include-runtime script.js`.
  
    return exports;
  }( // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
  (typeof module === "undefined" ? "undefined" : _typeof(module)) === "object" ? module.exports : {});
  
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    // This module should not be running in strict mode, so the above
    // assignment should always work unless something is misconfigured. Just
    // in case runtime.js accidentally runs in strict mode, we can escape
    // strict mode using a global Function call. This could conceivably fail
    // if a Content Security Policy forbids using Function, but in that case
    // the proper solution is to fix the accidental strict mode problem. If
    // you've misconfigured your bundler to force strict mode and applied a
    // CSP to forbid Function, and you're not willing to fix either of those
    // problems, please detail your unique predicament in a GitHub issue.
    Function("r", "regeneratorRuntime = r")(runtime);
  }
  
  },{}]},{},[1]);
  